package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
    private List<Pessoa> contatos = new ArrayList<>();

    public void adicionarContato(Pessoa pessoa){
        contatos.add(pessoa);
    }

    public void removerContato(String email){
        for(int i = 0; i < contatos.size(); i++){
            boolean removi = false;
            if(email.equals(contatos.get(i).getEmail())){
                contatos.remove(i);
                removi = true;
            }
            else if(i >= contatos.size() && removi == false){
                System.out.println("Não encontrei o email informado");
            }
        }
    }

    public List<Pessoa> getContatos() {
        return contatos;
    }

    public void setContatos(List<Pessoa> contatos) {
        this.contatos = contatos;
    }

    public Agenda() {}
}
