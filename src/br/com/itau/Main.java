package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Agenda agenda = new Agenda();

        int qtdPessoas = IO.solicitarQtdePessoas();

        for(int i = 0;i < qtdPessoas;i++){
            String nome = IO.solicitarNomeContato();
            String email = IO.solicitarEmailContato();
            Pessoa pessoa = new Pessoa(
                    nome,
                    email
            );
            agenda.adicionarContato(pessoa);
        }
        IO.imprimirContatos(agenda);

        String emailRemover = IO.solicitarEmailParaRemover();
        agenda.removerContato(emailRemover);
        IO.imprimirContatos(agenda);

        String emailImprimir = IO.solicitarEmailParaImprimir();
        IO.imprimirPessoaPorEmail(emailImprimir,agenda);
    }
}
