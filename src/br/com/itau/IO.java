package br.com.itau;

import java.util.Scanner;

public class IO {

    public static int solicitarQtdePessoas(){
        System.out.println("Digite quantas pessoas quer adicionar");
        Scanner scanner = new Scanner(System.in);
        int qtdPessoas = scanner.nextInt();
        return qtdPessoas;
    }

    public static String solicitarNomeContato(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o nome do contato:");
        String nome = scanner.nextLine();
        return nome;
    }

    public static String solicitarEmailContato(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o email do contato:");
        String email = scanner.nextLine();
        return email;
    }

    public static String solicitarEmailParaRemover(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o email do contato que deseja remover:");
        String email = scanner.nextLine();
        return email;
    }

    public static String solicitarEmailParaImprimir(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o email do contato que deseja imprimir:");
        String email = scanner.nextLine();
        return email;
    }

    public static void imprimirContatos(Agenda agenda){
        for (int i = 0;i < agenda.getContatos().size(); i++) {
            System.out.println("Contato: " + agenda.getContatos().get(i).getNome());
            System.out.println("Email: " + agenda.getContatos().get(i).getEmail());
            System.out.println("");
        }
    }

    public static void imprimirPessoaPorEmail(String email, Agenda agenda){
        for(int i = 0; i < agenda.getContatos().size(); i++){
            if(email.equals(agenda.getContatos().get(i).getEmail())){
                System.out.println("Contato: " + agenda.getContatos().get(i).getNome());
                System.out.println("Email: " + agenda.getContatos().get(i).getEmail());
                System.out.println("");
            }
        }
    }
}
